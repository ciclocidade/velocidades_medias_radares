import pandas as pd
import numpy as np
import geopandas as gpd
import glob
import warnings
warnings.filterwarnings('ignore')
import matplotlib.pyplot as plt
import seaborn as sns
from mobi import auxiliars
from datetime import datetime
from adjustText import adjust_text


###########################################################################################
# FUNÇÕES PARA PROCESSAMENTO DOS DADOS DOS CORREDORES
###########################################################################################

def processamento_dados_radar(caminho, corredor, subtrecho):
    '''

    :param pasta: Indique o caminho do arquivo 'Consolidação_Localização_Radares_sem_outliers.xlsx'
    :param corredor: string do número do corredor ('01', '02', ... '23'
    :param subtrecho: None, se o corredor não tiver subtrechos (Exemplo: Marginais Tietê e Pinheiros)
    :return:
    '''

    # 1. Leitura dos dados dos corredores
    dados_corredores = pd.read_excel(caminho, sheet_name = 'cod_local', index_col=[0])
    dados_corredores['cod_local'] = dados_corredores['cod_local'].astype(str)
    dados_corredores['corredor'] = dados_corredores['corredor'].astype(str)
    infos_geral = pd.DataFrame()
    pasta_input = 'D:\\Ciclocidade\\Inputs\\CORRED_' + corredor + '\\' # Crie um diretório nesse formato, em que cada pasta deve conter os arquivos diários das leituras dos radares de cada corredor
    trechos_total_dia = pd.DataFrame()

    for name in glob.glob(pasta_input + '*'): # For loop para processar um arquivo diário de cada vez
        trechos_total_dia = pd.DataFrame()
        data = name[-12:-4]
        print('Processando para o dia ', data)

        # 1. Filtrar dados corredores para o corredor desejado (Se o corredor é dividido
        print('Passo 1')
        if subtrecho == "":
            info_corredor = dados_corredores[dados_corredores['corredor'] == corredor]
        else:
            info_corredor = dados_corredores[dados_corredores['corredor'] == corredor + subtrecho]

        # 2. Dicionário cod_local - radar
        print('Passo 2')
        dict_cod_local_radar = pd.read_excel(caminho, sheet_name='lista_radares').dropna()
        dict_cod_local_radar['cod_local'] = dict_cod_local_radar['cod_local'].astype(str)
        dict_cod_local_radar['radar'] = dict_cod_local_radar['radar'].astype(int).astype(str)
        dict_cod_local_radar['corredor'] = dict_cod_local_radar['corredor'].astype(int)
        dict_cod_local_radar['corredor'] = np.where(dict_cod_local_radar['corredor']<10, '0' + dict_cod_local_radar['corredor'].astype(str),
                                                    dict_cod_local_radar['corredor'])

        # 3. Leitura dos dados dos radares do dia; inclusão do cod_local no dados_dia
        print('Passo 3')
        dados_dia = pd.read_csv(name, sep=';')
        dados_dia['local']=dados_dia['local'].astype(str)
        dados_dia = pd.merge(dados_dia, dict_cod_local_radar,
                             left_on = 'local', right_on = 'radar', how = 'left')

        dados_dia = dados_dia[dados_dia['data'].astype(str)==name[-12:-4]]


        # 4. Filtrar de dados_dia o corredor desejado
        print('Passo 4')
        dados_dia = dados_dia[dados_dia['cod_local'].isin(info_corredor['cod_local'])]

        for sentido in np.arange(1,3,1): # For loop para processar o corredor por sentido (1 ou 2)
            print('Sentido ', sentido)
            # 5. Inserir dados do corredor
            print('Passo 5, sentido', sentido)
            dados_corredor = info_corredor[info_corredor['sentido_codigo']==sentido]
            dados_dia_sentido = dados_dia[dados_dia['cod_local'].isin(dados_corredor['cod_local'])]

            # 5. Corrigir coluna hora e criar coluna data_hora
            print('Passo 6, sentido', sentido)
            dados_dia_sentido.loc[:, 'hora'] = '000000' + dados_dia_sentido['hora'].astype(str)
            dados_dia_sentido.loc[:, 'hora'] = dados_dia_sentido.loc[:, 'hora'].str[-6:]
            dados_dia_sentido.loc[:, 'hora'] = dados_dia_sentido.loc[:, 'hora'].str[:2] + dados_dia_sentido.loc[:, 'hora'].str[
                                                                          2:4] + dados_dia_sentido.loc[:, 'hora'].str[4:]
            dados_dia_sentido.loc[:, 'data'] = dados_dia_sentido.loc[:, 'data'].astype(str)
            dados_dia_sentido.loc[:, 'data'] = dados_dia_sentido.loc[:, 'data'].str[:4] + dados_dia_sentido.loc[:, 'data'].str[
                                                                          4:6] + dados_dia_sentido.loc[:, 'data'].str[6:]
            dados_dia_sentido['data_hora'] = dados_dia_sentido['data'] + dados_dia_sentido['hora']
            dados_dia_sentido.loc[:, 'data_hora'] = pd.to_datetime(dados_dia_sentido.loc[:, 'data_hora'], format='%Y%m%d%H%M%S')

            # Verifica se está faltando dados de algum radar para esse dia-sentido
            if len(dados_dia_sentido['cod_local'].unique())!= len(dados_corredor['cod_local'].unique()):
                print('No dia', name[-12:-4], 'está faltando o cod_local',
                      dados_corredor[~dados_corredor['cod_local'].isin(dados_dia_sentido['cod_local'])]['cod_local'].unique(),
                      'no sentido', sentido)



            dados_dia_sentido = pd.merge(dados_dia_sentido, dados_corredor[['cod_local', 'sequencia','dist_proximo',
                                                                            'tempo_maximo_trecho_leve', 'tempo_maximo_trecho_pesado']],
            on = 'cod_local', how = 'left')

            if len(dados_dia_sentido)==0:
                print('Não há informações sobre o sentido ', sentido)

            else:
                # 7. Quantidade de trechos para fazer as análise por trecho
                print('Passo 7, sentido', sentido)
                qtde_trechos = int(dados_dia_sentido['sequencia'].max())  #

                # 8. Informações gerais (total de observações)
                print('Passo 8, sentido', sentido)
                info_geral = dados_dia_sentido.groupby(['data', 'cod_local']).size().reset_index(name='volume_total_dia')

                # 9. Informações gerais (total de com placa e sem placa)
                print('Passo 9, sentido', sentido)
                dados_dia_sentido = dados_dia_sentido[dados_dia_sentido['placa']!='       ']
                dados_dia_sentido = dados_dia_sentido.dropna(subset='placa')
                com_placa = dados_dia_sentido.groupby(['data', 'cod_local']).size().reset_index(name='volume_total_dia_com_placa')
                info_geral = pd.merge(info_geral, com_placa,
                                          on = ['data', 'cod_local'], how = 'left')
                info_geral['perc_volume_total_dia_com_placa'] = round((info_geral['volume_total_dia_com_placa'] / info_geral['volume_total_dia'])*100,1)
                info_geral['volume_total_dia_sem_placa'] = info_geral['volume_total_dia'] - info_geral['volume_total_dia_com_placa']
                info_geral['perc_volume_total_dia_sem_placa'] = 100 - info_geral['perc_volume_total_dia_com_placa']

                # 10. Informações gerais (Número de placas distintas)
                print('Passo 10, sentido', sentido)
                placas_distintas = dados_dia_sentido.groupby(['data', 'cod_local', 'placa']).size().reset_index()
                placas_distintas = placas_distintas.groupby(['data', 'cod_local']).size().reset_index(name='n_placas_distintas')
                info_geral = pd.merge(info_geral, placas_distintas,
                                          on = ['data', 'cod_local'], how = 'left')

                # 11. Informações gerais (Divisão Modal)
                print('Passo 11, sentido', sentido)
                tipo_veic = dados_dia_sentido.groupby(['data', 'cod_local'])['tipo_veic'].value_counts(normalize = True).reset_index(name = 'perc_tipo_veic')
                tipo_veic = pd.pivot_table(tipo_veic, values = 'perc_tipo_veic', index = ['data', 'cod_local'], columns = ['tipo_veic']).reset_index().fillna(0)
                tipo_veic.rename(columns = {0: 'perc_moto',
                                                1: 'perc_auto',
                                                2: 'perc_onibus',
                                                3: 'perc_caminhao'}, inplace = True)
                tipo_veic.iloc[:,2:] = round(tipo_veic.iloc[:,2:]*100,1)
                info_geral = pd.merge(info_geral, tipo_veic,
                                          on=['data', 'cod_local'], how='left')

                # 12. Análise da velocidade dos veículos
                print('Passo 12, sentido', sentido)
                dados_dia_sentido = dados_dia_sentido.sort_values(by = ['placa', 'data_hora']).reset_index(drop=True)
                dados_dia_sentido['placa_proximo'] = dados_dia_sentido['placa'].shift(-1)
                dados_dia_sentido['sequencia_proximo'] = dados_dia_sentido['sequencia'].shift(-1)
                dados_dia_sentido['cod_local_proximo'] = dados_dia_sentido['cod_local'].shift(-1)
                dados_dia_sentido['data_hora_proximo'] = dados_dia_sentido['data_hora'].shift(-1)
                dados_dia_sentido['vel_p_kmh'] = (dados_dia_sentido['vel_p'].astype(float)/10)*3.6
                dados_dia_sentido['vel_p_proximo_kmh'] = dados_dia_sentido['vel_p_kmh'].shift(-1)

                # 12.1 A placa do próximo registro deve ser do mesmo veículo
                dados_dia_sentido = dados_dia_sentido[dados_dia_sentido['placa']==dados_dia_sentido['placa_proximo']]

                # 12.2 A sequência do radar do próximo registro deve ser o imediatamente superior
                dados_dia_sentido = dados_dia_sentido[dados_dia_sentido['sequencia'] == dados_dia_sentido['sequencia_proximo']-1].reset_index(drop=True)

                # 12.3 Cálculo do tempo de viagem entre os radares
                dados_dia_sentido['tempo_viagem_trecho'] = (dados_dia_sentido['data_hora_proximo'] - dados_dia_sentido['data_hora'])
                dados_dia_sentido['tempo_viagem_trecho'] = dados_dia_sentido['tempo_viagem_trecho'].apply(lambda x: x.total_seconds())

                # 12.4 Velocidade média no trecho
                dados_dia_sentido['velocidade_media_trecho'] = round((1000 * dados_dia_sentido['dist_proximo'] / dados_dia_sentido['tempo_viagem_trecho'])*3.6,1)

                # 12.5 Diferenciar se é a mesma viagem (critério tempo máximo)
                dados_dia_sentido['tempo_viagem_trecho'] = dados_dia_sentido['tempo_viagem_trecho']  / 60
                dados_dia_sentido = dados_dia_sentido[dados_dia_sentido['tempo_viagem_trecho'] < dados_dia_sentido['tempo_maximo_trecho_pesado']]

                # 13. Número de registros válidos do radar à jusante (e montante) que obtiveram contagem
                print('Passo 13, sentido', sentido)
                volume_radar_montante = dados_dia_sentido.groupby(['data', 'cod_local']).size().reset_index(name= 'volume_montante')
                volume_radar_jusante = dados_dia_sentido.groupby(['data', 'cod_local_proximo']).size().reset_index(name= 'volume_jusante')
                info_geral = pd.merge(info_geral, volume_radar_montante,
                                          on = ['data', 'cod_local'], how='left')
                # Incluir volume do radar à jusante
                info_geral = pd.merge(info_geral, volume_radar_jusante,
                                          left_on=['data', 'cod_local'], right_on = ['data', 'cod_local_proximo'],
                                          how = 'left').reset_index(drop=True)
                del info_geral['cod_local_proximo']

                info_geral['sentido'] = sentido
                info_geral = pd.merge(info_geral, dados_corredor [['cod_local', 'sequencia']],
                                      on = 'cod_local', how = 'left')

                # Gerar dataframes sobre os registros que possuem dados coerentes no radar à jusante e à montante
                print('Passo 14, sentido', sentido)
                if len(dados_dia_sentido)!=0:
                    for i in np.arange(1, qtde_trechos, 1):
                        trecho = dados_dia_sentido[dados_dia_sentido['sequencia']==i].reset_index(drop=True)
                        trecho['sentido'] = sentido
                        trecho['trecho'] = i
                        trechos_total_dia = pd.concat([trechos_total_dia, trecho]).reset_index(drop=True)


                else:
                    print('Trecho descaracterizado por falta de dados contínuos entre radares')

                infos_geral = pd.concat([infos_geral, info_geral]).sort_values(by=['data','cod_local']).reset_index(drop=True)

        # Salvar dados dos trechos
        print('Salvando trecho dia', data)

        if subtrecho == '':
            trechos_total_dia.to_csv(
                'D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' +
                corredor + '\\trechos_corredor_' + corredor + '_' +  data + '.csv', float_format='%.2f')

        else:
            trechos_total_dia.to_csv(
                'D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' +
                corredor + '\\trechos_corredor_' + corredor + subtrecho + '_' + data + '.csv', float_format='%.2f')

    # Salvar informações gerais
    if subtrecho == '':
        infos_geral.to_csv('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' +
                           corredor + '\\info_geral_radares_CORRED_' + corredor + '.csv', float_format='%.2f')
    else:
        infos_geral.to_csv('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' +
                           corredor + '\\info_geral_radares_CORRED_' + corredor + subtrecho + '.csv', float_format='%.2f')
    return



def match_functionamento_simultaneo(info_geral):
    '''
    Verifica se dois radares consecutivos estão funcionando em dias simultâneos
    :param info_geral:
    :return:
    '''
    simultaneo = pd.DataFrame()

    for data in info_geral['data'].unique():
        info_geral_dia = info_geral[info_geral['data'] == data].sort_values(by= ['sentido', 'sequencia'])
        info_geral_dia['prox_cod_local'] = info_geral_dia['cod_local'].shift(-1)
        info_geral_dia['prox_sentido'] = info_geral_dia['sentido'].shift(-1)
        info_geral_dia['prox_sequencia'] = info_geral_dia['sequencia'].shift(-1)
        info_geral_dia['prox_volume_total_dia'] = info_geral_dia['volume_total_dia'].shift(-1)
        info_geral_dia['prox_volume_total_dia_com_placa'] = info_geral_dia['volume_total_dia_com_placa'].shift(-1)

        info_geral_dia = info_geral_dia[info_geral_dia['sentido'] == info_geral_dia['prox_sentido']]
        info_geral_dia = info_geral_dia[info_geral_dia['sequencia'] == info_geral_dia['prox_sequencia'] - 1]

        simultaneo = pd.concat([simultaneo, info_geral_dia])

    return simultaneo



# Gera estatísticas gerais dos dados brutos (informações gerais
def gerar_estatisticas_dados_brutos_radares(corredor, subtrecho):
    '''

    :param corredor:
    :return:
    '''

    print('Gerando estatísticas dados brutos do corredor ', corredor)
    local_arquivo = 'D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' + corredor + '\\info_geral_radares_CORRED_'
    if subtrecho == '':
        arquivo = local_arquivo + corredor + '.csv'
    else:
        arquivo = local_arquivo + corredor + subtrecho + '.csv'

    info_geral = pd.read_csv(arquivo, index_col = [0])

    # Consertar output do dado de entrada
    info_geral.rename(columns={
            'volume_montante':'volume_jusante_1',
            'volume_jusante':'volume_montante_1'}, inplace = True)

    info_geral.rename(columns={
            'volume_jusante_1': 'volume_jusante',
            'volume_montante_1': 'volume_montante'}, inplace=True)

    # 1. Total de dias sem registros
    dados_brutos = (365 - info_geral.groupby(['cod_local', 'sentido', 'sequencia']).size()).reset_index(name = 'dias_sem_registro').sort_values(by = ['sentido', 'sequencia'])

    # 2. Total de registros no ano
    total_reg_ano = info_geral.groupby('cod_local')['volume_total_dia'].sum().reset_index(name = 'tot_reg_ano')
    dados_brutos = dados_brutos.merge(total_reg_ano, on='cod_local', how = 'left')

    # 3. Total de registros com placa
    total_reg_com_placa = info_geral.groupby('cod_local')['volume_total_dia_com_placa'].sum().reset_index(name = 'tot_reg_com_placa_ano')
    dados_brutos = dados_brutos.merge(total_reg_com_placa, on='cod_local', how='left')
    dados_brutos['perc_reg_com_placa_ano'] = round((dados_brutos['tot_reg_com_placa_ano'] / dados_brutos['tot_reg_ano'])*100,1)
    dados_brutos['perc_reg_sem_placa_ano'] = 100 - dados_brutos['perc_reg_com_placa_ano']

    # 4. Divisão modal (apenas de veículos com placa)
    info_geral['tot_moto'] = info_geral['perc_moto']*info_geral['volume_total_dia_com_placa']
    info_geral['tot_auto'] = info_geral['perc_auto'] * info_geral['volume_total_dia_com_placa']
    info_geral['tot_onibus'] = info_geral['perc_onibus'] * info_geral['volume_total_dia_com_placa']
    info_geral['tot_caminhao'] = info_geral['perc_caminhao'] * info_geral['volume_total_dia_com_placa']

    divisao_modal = info_geral.groupby('cod_local')[['tot_moto', 'tot_auto', 'tot_onibus', 'tot_caminhao']].sum().reset_index()
    divisao_modal = divisao_modal.merge(total_reg_com_placa, on= 'cod_local', how = 'left')
    divisao_modal['perc_moto'] = round(divisao_modal['tot_moto'] / divisao_modal['tot_reg_com_placa_ano'],1)
    divisao_modal['perc_auto'] = round(divisao_modal['tot_auto'] / divisao_modal['tot_reg_com_placa_ano'], 1)
    divisao_modal['perc_onibus'] = round(divisao_modal['tot_onibus'] / divisao_modal['tot_reg_com_placa_ano'], 1)
    divisao_modal['perc_caminhao'] = 100 - divisao_modal['perc_moto'] - divisao_modal['perc_auto'] - divisao_modal['perc_onibus']

    dados_brutos = dados_brutos.merge(divisao_modal[['cod_local', 'perc_moto', 'perc_auto', 'perc_onibus', 'perc_caminhao']],
                                          on = 'cod_local', how = 'left')

    # 5. Média de placas distintas por dia
    n_placas_distintas = round(info_geral.groupby('cod_local')['n_placas_distintas'].mean().reset_index(name = 'media_placas_distintas_dia'))
    dados_brutos = dados_brutos.merge(n_placas_distintas, on='cod_local', how='left')

    # Simultaneo
    # Análise perda de dados nos dias em que os dois radares consecutivos functionaram nos mesmos dias
    dados_brutos['prox_cod_local'] = dados_brutos['cod_local'].shift(-1)
    dados_brutos['prox_dias_sem_registro'] = dados_brutos['dias_sem_registro'].shift(-1)
    dados_brutos['prox_tot_reg_ano'] = dados_brutos['tot_reg_ano'].shift(-1)
    dados_brutos['prox_tot_reg_com_placa_ano'] = dados_brutos['tot_reg_com_placa_ano'].shift(-1)

    info_geral_simultaneos = match_functionamento_simultaneo(info_geral)

    #Quantidade de dias que os radares consecutivos funcionam simultanemente
    qte_dias_funciona_simultaneo = info_geral_simultaneos.groupby('cod_local').size().reset_index(name = 'qte_dias_funciona_simultaneo_prox')
    dados_brutos = dados_brutos.merge(qte_dias_funciona_simultaneo, on='cod_local', how = 'left')

    # Perda de dados pelo não funcionamento simultaneo dos radares
    perda_dados = info_geral_simultaneos.groupby('cod_local')[['volume_total_dia', 'volume_total_dia_com_placa', 'prox_volume_total_dia', 'prox_volume_total_dia_com_placa']].sum().reset_index()
    perda_dados.columns = ['cod_local', 'tot_reg_ano_simu', 'tot_reg_com_placa_ano_simu', 'prox_tot_reg_ano_simu', 'prox_tot_reg_com_placa_ano_simu']
    dados_brutos = dados_brutos.merge(perda_dados, on = 'cod_local', how = 'left')

    dados_brutos['perda_tot_reg_simu'] = dados_brutos['tot_reg_ano'] - dados_brutos['tot_reg_ano_simu']
    dados_brutos['perc_perda_tot_reg_simu'] = round((dados_brutos['perda_tot_reg_simu'] / dados_brutos['tot_reg_ano'])*100)
    dados_brutos['perda_tot_reg_com_placa_simu'] = dados_brutos['tot_reg_com_placa_ano'] - dados_brutos['tot_reg_com_placa_ano_simu']
    dados_brutos['perc_perda_tot_reg_com_placa_simu'] = round((dados_brutos['perda_tot_reg_com_placa_simu'] / dados_brutos['tot_reg_com_placa_ano']) * 100)
    dados_brutos['perda_prox_tot_reg_simu'] = dados_brutos['prox_tot_reg_ano'] - dados_brutos['prox_tot_reg_ano_simu']
    dados_brutos['perc_perda_prox_tot_reg_simu'] = round((dados_brutos['perda_prox_tot_reg_simu'] / dados_brutos['prox_tot_reg_ano']) * 100)
    dados_brutos['perda_prox_tot_reg_com_placa_simu'] = dados_brutos['prox_tot_reg_com_placa_ano'] - dados_brutos['prox_tot_reg_com_placa_ano_simu']
    dados_brutos['perc_perda_prox_tot_reg_com_placa_simu'] = round((dados_brutos['perda_prox_tot_reg_com_placa_simu'] / dados_brutos['prox_tot_reg_com_placa_ano']) * 100)

    dados_brutos = dados_brutos[['cod_local', 'sentido', 'sequencia', 'dias_sem_registro', 'tot_reg_ano',
                                     'tot_reg_com_placa_ano', 'perc_reg_com_placa_ano',
                                     'perc_reg_sem_placa_ano', 'perc_moto', 'perc_auto', 'perc_onibus',
                                     'perc_caminhao', 'media_placas_distintas_dia', 'prox_cod_local',
                                     'prox_dias_sem_registro','qte_dias_funciona_simultaneo_prox', 'prox_tot_reg_ano',
                                     'prox_tot_reg_com_placa_ano','tot_reg_ano_simu', 'perda_tot_reg_simu', 'perc_perda_tot_reg_simu',
                                     'tot_reg_com_placa_ano_simu', 'perda_tot_reg_com_placa_simu', 'perc_perda_tot_reg_com_placa_simu',
                                     'prox_tot_reg_ano_simu', 'perda_prox_tot_reg_simu', 'perc_perda_prox_tot_reg_simu',
                                     'prox_tot_reg_com_placa_ano_simu', 'perda_prox_tot_reg_com_placa_simu', 'perc_perda_prox_tot_reg_com_placa_simu']]

    # Salvar estatísticas dos dados brutos (informações gerais dos radares)
    if subtrecho == '':
        dados_brutos.to_csv("D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_" + corredor + '\\dados_brutos_CORRED_' + corredor + '.csv',
                            float_format='%.2f')
    else:
        dados_brutos.to_csv(
            "D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_" + corredor + '\\dados_brutos_CORRED_' + corredor + subtrecho + '.csv',
            float_format='%.2f')

    return




def gerar_estatisticas_por_trecho(caminho, corredor, subtrecho):
    '''
    Lê dados dos trechos e gera suas respectivas estatísticas
    :param corredor:
    :return:
    '''
    print('Gerando estatísticas dos trechos do corredor ', corredor)
    pasta_input = 'D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' + corredor + '\\'
    velocidades_permitidas = pd.read_excel(caminho, sheet_name='cod_local',
                                           usecols=['cod_local', 'velocidade_carro_moto', 'velocidade_cam_oni'])
    velocidades_permitidas['cod_local'] = velocidades_permitidas['cod_local'].astype(str)

    # Cria dataframes vazios para serem preenchidos com as estatísticas
    resumo_dados_trechos = pd.DataFrame()
    resumo_total_infra_mes_corredor = pd.DataFrame()
    resumo_infra_mensal_trechos = pd.DataFrame()
    resumo_analise_dia_semana_corredor = pd.DataFrame()
    resumo_analise_dia_semana_por_trecho = pd.DataFrame()
    resumo_analise_hora_dia_corredor = pd.DataFrame()
    resumo_analise_hora_dia_trecho = pd.DataFrame()


    # chave dia da semana
    chave_dia_semana = {0: 'Segunda-feira',
                        1: 'Terça-feira',
                        2: 'Quarta-feira',
                        3: 'Quinta_feira',
                        4: 'Sexta-feira',
                        5: 'Sábado',
                        6: 'Domingo'}
    if subtrecho == '':
        nomes_arquivos = glob.glob(pasta_input + 'trechos_corredor_'+ corredor + '*')
    else:
        nomes_arquivos = glob.glob(pasta_input + 'trechos_corredor_' + corredor + subtrecho + '*')

    # Para cada arquivo relativos aos trechos
    for name in nomes_arquivos:
        try:
            data = name[-12:-4]
            print('Lendo dia', data)

            # Leitura dos dados do trecho para o dia
            dados_dia = pd.read_csv(name, index_col = [0])
            dados_dia = dados_dia.reset_index(drop=True)
            dados_dia['cod_local'] = dados_dia['cod_local'].astype(str)
            dados_dia['cod_local_proximo'] = dados_dia['cod_local_proximo'].astype(str)

            # Velocidade permitida do trecho igual à velocidade do radar à jusante
            dados_dia = dados_dia.merge(velocidades_permitidas, left_on = 'cod_local_proximo',
                                                      right_on = 'cod_local', how='left')
            dados_dia['velocidade_permitida'] = np.where((dados_dia['tipo_veic'] == 0) |
                                                                 (dados_dia['tipo_veic'] == 1),
                                                                 dados_dia['velocidade_carro_moto'],
                                                                 dados_dia['velocidade_cam_oni'])
            dados_trechos = dados_dia.groupby(['trecho', 'sentido']).first().reset_index()[['trecho', 'sentido', 'velocidade_permitida']].sort_values(by = ['sentido', 'trecho'])
            dados_trechos['data'] = data

            # 1. Total de registros
            print('Passo 1')
            total_registros = dados_dia.groupby(['trecho', 'sentido']).size().reset_index(name = 'total_registros_ano')
            dados_trechos = dados_trechos.merge(total_registros, on = ['trecho', 'sentido'], how ='left')

            # 2. Análises velocidade média x velocidade instantânea
            # Verifica se houve infração de velocidade nos radares à montante, jusante e de velocidade média
            print('Passo 2')
            dados_dia['infracao_radar_montante'] = dados_dia['vel_p_kmh'] > dados_dia['velocidade_permitida']
            dados_dia['infracao_radar_jusante'] = dados_dia['vel_p_proximo_kmh'] > dados_dia['velocidade_permitida']
            dados_dia['infracao_velocidade_media'] = dados_dia['velocidade_media_trecho'] > dados_dia['velocidade_permitida']

            # Total de infrações
            total_infracoes = dados_dia.groupby(['sentido', 'trecho'])[['infracao_radar_montante', 'infracao_radar_jusante', 'infracao_velocidade_media']].sum().reset_index()
            dados_trechos = dados_trechos.merge(total_infracoes, on=['sentido', 'trecho'], how = 'left')


            # 3. Percentual de registros que fizeram apenas no radar à montante, apenas à jusante, apenas radar pontual,
            # apenas velocidade média e em todos os tipos
            print('Passo 3')
            dados_dia['infra_apenas_montante'] = np.where((dados_dia['infracao_radar_montante'] == True)&
                                                                   (dados_dia['infracao_radar_jusante'] == False)&
                                                                   (dados_dia['infracao_velocidade_media'] == False), 1, 0)

            dados_dia['infra_apenas_jusante'] = np.where((dados_dia['infracao_radar_montante'] == False)&
                                                                  (dados_dia['infracao_radar_jusante'] == True) &
                                                                   (dados_dia['infracao_velocidade_media'] == False), 1, 0)

            dados_dia['infra_apenas_vel_instantanea'] = dados_dia['infra_apenas_montante'] + dados_dia['infra_apenas_jusante']

            dados_dia['infra_apenas_vel_media'] = np.where(np.logical_or(dados_dia['infracao_radar_montante'] == False,
                                                                                  dados_dia['infracao_radar_jusante'] == False) &
                                                                    (dados_dia['infracao_velocidade_media'] == True), 1, 0)

            dados_dia['infra_todos'] = np.where((dados_dia['infracao_radar_montante'] == True) &
                                                                    (dados_dia['infracao_radar_jusante'] == True) &
                                                                    (dados_dia['infracao_velocidade_media'] == True), 1, 0)

            tipos_infracao = dados_dia.groupby(['sentido', 'trecho'])[['infra_apenas_montante', 'infra_apenas_jusante',
                                                                               'infra_apenas_vel_instantanea', 'infra_apenas_vel_media', 'infra_todos']].sum().reset_index()

            dados_trechos = dados_trechos.merge(tipos_infracao, on = ['sentido', 'trecho'], how = 'left')
            resumo_dados_trechos = pd.concat([resumo_dados_trechos, dados_trechos]).reset_index(drop=True)

            if subtrecho != '':
                resumo_dados_trechos['subtrecho'] = subtrecho


            # 4. Análise sazonalidade
            print('Passo 4')
            # 4.1 Meses do ano
            dados_dia['data_hora'] = pd.to_datetime(dados_dia['data_hora'])
            dados_dia['mes'] = dados_dia['data_hora'].dt.month

            # 4.1.1 Análise do corredor inteiro
            total_infra_mes_corredor = dados_dia.groupby('mes').size().reset_index(name = 'total_registros')

            infra_mensal = dados_dia.groupby(['mes'])[['infracao_radar_montante', 'infracao_radar_jusante',
                                                                'infracao_velocidade_media']].sum()

            total_infra_mes_corredor  = total_infra_mes_corredor.merge(infra_mensal , on='mes', how='left')
            total_infra_mes_corredor['data'] = data

            resumo_total_infra_mes_corredor = pd.concat([resumo_total_infra_mes_corredor, total_infra_mes_corredor]).reset_index(drop=True)


            # 4.1.2 Análise por trechos do corredor
            total_infra_mes_trechos = dados_dia.groupby(['sentido', 'trecho', 'mes']).size().reset_index(name='total_registros')

            infra_mensal_trechos = dados_dia.groupby(['sentido', 'trecho', 'mes'])[['infracao_radar_montante', 'infracao_radar_jusante',
                                                                'infracao_velocidade_media']].sum()
            total_infra_mes_trechos = total_infra_mes_trechos.merge(infra_mensal_trechos, on = ['sentido', 'trecho', 'mes'],
                                                                    how = 'left').sort_values( by =['sentido', 'trecho'])
            total_infra_mes_trechos['data'] = data
            resumo_infra_mensal_trechos = pd.concat([resumo_infra_mensal_trechos, total_infra_mes_trechos]).reset_index(
                drop=True)



            # 4.2 Análise por dia da semana
            dados_dia['dia_semana'] = dados_dia['data_hora'].dt.weekday.map(chave_dia_semana)

            # 4.2.1 Corredor inteiro
            analise_dia_semana_corredor = dados_dia['dia_semana'].value_counts().reset_index(name = 'tot_reg_dia_semana')
            infra_dia_semana_corredor = dados_dia.groupby(['dia_semana'])[['infracao_radar_montante', 'infracao_radar_jusante',
                                                                           'infracao_velocidade_media']].sum()
            analise_dia_semana_corredor = analise_dia_semana_corredor.merge(infra_dia_semana_corredor, on = 'dia_semana', how = 'left')
            analise_dia_semana_corredor['data'] = data
            resumo_analise_dia_semana_corredor = pd.concat([resumo_analise_dia_semana_corredor, analise_dia_semana_corredor]).reset_index(drop=True)


            # 4.2.2 Por trechos
            analise_dia_semana_por_trecho = dados_dia.groupby(['sentido', 'trecho', 'dia_semana']).size().reset_index(name = 'tot_reg_dia_semana')
            infra_dia_semana_por_trecho = dados_dia.groupby(['sentido', 'trecho', 'dia_semana'])[['infracao_radar_montante', 'infracao_radar_jusante',
                                                                           'infracao_velocidade_media']].sum()
            analise_dia_semana_por_trecho = analise_dia_semana_por_trecho.merge(infra_dia_semana_por_trecho,
                                                                                    on = ['sentido', 'trecho', 'dia_semana'],
                                                                                    how = 'left')
            analise_dia_semana_por_trecho['data'] = data
            resumo_analise_dia_semana_por_trecho = pd.concat([resumo_analise_dia_semana_por_trecho, analise_dia_semana_por_trecho]).reset_index(drop=True)


            # 4.3 Análise por horário do dia
            # 4.3.1 Analise do corredor
            dados_dia['hora_dia'] = dados_dia['data_hora'].dt.hour
            analise_hora_dia_corredor = dados_dia.groupby(['hora_dia']).size().reset_index(name = 'tot_reg_hora')
            infra_hora_dia = dados_dia.groupby(['hora_dia'])[['infracao_radar_montante', 'infracao_radar_jusante',
                                                                           'infracao_velocidade_media']].sum()
            analise_hora_dia_corredor =analise_hora_dia_corredor.merge(infra_hora_dia, on = 'hora_dia', how = 'left')
            analise_hora_dia_corredor['data'] = data
            resumo_analise_hora_dia_corredor = pd.concat([resumo_analise_hora_dia_corredor, analise_hora_dia_corredor]).reset_index(drop=True)

            # 4.3.2 Análise por trecho
            analise_hora_dia_trecho = dados_dia.groupby(['sentido', 'trecho', 'hora_dia']).size().reset_index(name='tot_reg_hora')
            infra_hora_dia_trecho = dados_dia.groupby(['sentido', 'trecho', 'hora_dia'])[['infracao_radar_montante', 'infracao_radar_jusante',
                                                                       'infracao_velocidade_media']].sum()
            analise_hora_dia_trecho = analise_hora_dia_trecho.merge(infra_hora_dia_trecho, on=['sentido', 'trecho', 'hora_dia'], how='left')
            analise_hora_dia_trecho['data'] = data

            resumo_analise_hora_dia_trecho = pd.concat([resumo_analise_hora_dia_trecho, analise_hora_dia_trecho]).reset_index(drop=True)

        except:
            print('Dia ', data, ' não possui dados')
            continue

    # Salvar arquivos das estatísticas
    resumo_dados_trechos.to_csv(pasta_input + 'resumo_dados_trechos_CORRED_' + corredor + subtrecho +'.csv', float_format='%.2f')
    resumo_total_infra_mes_corredor.to_csv(pasta_input + 'resumo_total_infra_mes_corredor_CORRED_' + corredor + subtrecho + '.csv', float_format='%.2f')
    resumo_infra_mensal_trechos.to_csv(pasta_input + 'resumo_infra_mensal_trechos_CORRED_' + corredor + subtrecho + '.csv', float_format='%.2f')
    resumo_analise_dia_semana_corredor.to_csv(pasta_input + 'resumo_analise_dia_semana_corredor_CORRED_' + corredor + subtrecho   + '.csv', float_format='%.2f')
    resumo_analise_dia_semana_por_trecho.to_csv(pasta_input + 'resumo_analise_dia_semana_por_trecho_CORRED_' + corredor + subtrecho + '.csv', float_format='%.2f')
    resumo_analise_hora_dia_corredor.to_csv(pasta_input + 'resumo_analise_hora_dia_corredor_CORRED_' + corredor + subtrecho + '.csv', float_format='%.2f')
    resumo_analise_hora_dia_trecho.to_csv(pasta_input + 'resumo_analise_hora_dia_trecho_CORRED_' + corredor + subtrecho + '.csv', float_format='%.2f')


    return

############################
# Processamentos - retirar o # para processr os dados de interesse
############################
# 1. Processar corredores (gerar dados dos trechos e salvar informações gerais)
import datetime
# #
corredores = ['27'] # ['01', '02',...]
subtrechos = ['a', 'b', 'c'] # None
#
for corredor in corredores:
    for subtrecho in subtrechos:
        # using now() to get current time
        print('#################################################')
        print('Iniciando corredor ', corredor)
        print('#################################################')
        pre = datetime.datetime.now()
        processamento_dados_radar(r"D:\Ciclocidade\Outputs\Base Corredores sem Outlier.xlsx",
                                  corredor, subtrecho )
        x = datetime.datetime.now()
        print('Tempo Processamento', x-pre)

# 2. Processar estatísticas dos corredores (informações gerais e dos trechos)
corredores = ['27'] # ['01', '02',...]
subtrechos = ['a', 'b', 'c'] # None
for corredor in corredores:
    for subtrecho in subtrechos:
        try:
            gerar_estatisticas_dados_brutos_radares(corredor, subtrecho)
            gerar_estatisticas_por_trecho(r"D:\Ciclocidade\Outputs\Base Corredores sem Outlier.xlsx",
                                      corredor, subtrecho)
        except:
            print('Erro no Corredor ', corredor)
            continue



##########################################
# Gerar Anexos
#########################################
# 1. Quantidade de dias sem funcionamento simultâneo e Percentual de registros com placa
corredores = ['23'] # ['01', '02',...]
subtrechos = [''] # None

caracteristicas = pd.read_excel(r"D:\Ciclocidade\Outputs\Corredores_Processados\ANEXOS\1. Resumo Corredores.xlsx",
                                sheet_name = 'Descrição Detalhada',
                                   usecols=['Nome Corredor', 'Corredor', 'Código dos Radares',
                                            'Sequência', 'Sentido'])

qtde_simultaneo = pd.DataFrame()
com_placa = pd.DataFrame()
for corredor in corredores:
    try:
        if corredor!='27':
            dados_brutos = pd.read_csv(r"D:\Ciclocidade\Outputs\Corredores_Processados\CORRED_" + corredor +
                                       "\dados_brutos_CORRED_" + corredor + ".csv")
            dados_brutos['cod_local'] = dados_brutos['cod_local'].astype(str)
            dados_brutos_simu = dados_brutos[['cod_local', 'qte_dias_funciona_simultaneo_prox']]
            dados_brutos_simu['nao_trabalha_dias_simultaneos'] = 365 - dados_brutos_simu['qte_dias_funciona_simultaneo_prox']
            qtde_simultaneo = pd.concat([qtde_simultaneo, dados_brutos_simu])

            dados_brutos_placa = dados_brutos[['cod_local', 'perc_reg_com_placa_ano', 'perc_reg_sem_placa_ano']]
            com_placa = pd.concat([com_placa, dados_brutos_placa])

        else:
            for subcorredor in ['a', 'b', 'c']:
                dados_brutos = pd.read_csv(r"D:\Ciclocidade\Outputs\Corredores_Processados\CORRED_" + corredor +
                                           "\dados_brutos_CORRED_" + corredor + subcorredor + ".csv")
                dados_brutos['cod_local'] = dados_brutos['cod_local'].astype(str)
                dados_brutos_simu = dados_brutos[['cod_local', 'qte_dias_funciona_simultaneo_prox']]
                dados_brutos_simu['nao_trabalha_dias_simultaneos'] = 365 - dados_brutos_simu[
                    'qte_dias_funciona_simultaneo_prox']
                qtde_simultaneo = pd.concat([qtde_simultaneo, dados_brutos_simu])

                dados_brutos_placa = dados_brutos[['cod_local', 'perc_reg_com_placa_ano', 'perc_reg_sem_placa_ano']]
                com_placa = pd.concat([com_placa, dados_brutos_placa])

    except:
        print('Erro no Corredor ', corredor)
        continue

caracteristicas['Código dos Radares'] = caracteristicas['Código dos Radares'].astype(str)

# Quantidade de dias que os radares trabalharam de simultaneamente
x1 = caracteristicas.merge(qtde_simultaneo, left_on='Código dos Radares', right_on='cod_local',
                                        how = 'left')
x1.to_excel('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\ANEXOS\\2. Dias trabalho Simultaneo_complemento.xlsx')

# Placas com registro
x2=  caracteristicas.merge(com_placa, left_on='Código dos Radares', right_on='cod_local',
                                        how = 'left')
x2.to_excel('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\ANEXOS\\3. Percentual de registros com placa_complemento.xlsx')

# 3. Análise das velocidades médias
# 2.1 Infrações
infracoes = pd.DataFrame()
for corredor in corredores:
    try:
        if corredor !='27':
            dados_trechos = pd.read_csv('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' + corredor +
                                        '\\resumo_dados_trechos_CORRED_' + corredor + '.csv', index_col=[0])
            dados_trechos = dados_trechos.groupby(['sentido', 'trecho'])[['total_registros_ano', 'infracao_radar_montante',
                                                                          'infracao_radar_jusante',
                                                                          'infracao_velocidade_media',
                                                                          'infra_apenas_montante', 'infra_apenas_jusante',
                                                                          'infra_apenas_vel_instantanea',
                                                                          'infra_apenas_vel_media',
                                                                          'infra_todos']].sum().reset_index()

            dados_trechos['perc_infra_montante'] = round(
                (dados_trechos['infracao_radar_montante'] / dados_trechos['total_registros_ano']) * 100, 1)
            dados_trechos['perc_infra_jusante'] = round(
                (dados_trechos['infracao_radar_jusante'] / dados_trechos['total_registros_ano']) * 100, 1)
            dados_trechos['perc_infracao_velocidade_media'] = round(
                (dados_trechos['infracao_velocidade_media'] / dados_trechos['total_registros_ano']) * 100, 1)

            dados_trechos['corredor'] = corredor

            infracoes = pd.concat([infracoes, dados_trechos])

        else:
            for subtrecho in ['a', 'b', 'c']:
                dados_trechos = pd.read_csv('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' + corredor +
                                            '\\resumo_dados_trechos_CORRED_' + corredor + subtrecho + '.csv', index_col=[0])
                dados_trechos = dados_trechos.groupby(['sentido', 'trecho'])[
                    ['total_registros_ano', 'infracao_radar_montante',
                     'infracao_radar_jusante',
                     'infracao_velocidade_media',
                     'infra_apenas_montante', 'infra_apenas_jusante',
                     'infra_apenas_vel_instantanea',
                     'infra_apenas_vel_media',
                     'infra_todos']].sum().reset_index()

                dados_trechos['perc_infra_montante'] = round(
                    (dados_trechos['infracao_radar_montante'] / dados_trechos['total_registros_ano']) * 100, 1)
                dados_trechos['perc_infra_jusante'] = round(
                    (dados_trechos['infracao_radar_jusante'] / dados_trechos['total_registros_ano']) * 100, 1)
                dados_trechos['perc_infracao_velocidade_media'] = round(
                    (dados_trechos['infracao_velocidade_media'] / dados_trechos['total_registros_ano']) * 100, 1)
                dados_trechos['corredor'] = corredor + subtrecho
                infracoes = pd.concat([infracoes, dados_trechos])
    except:
        print('Erro no Corredor ', corredor)
        continue

infracoes.to_excel('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\ANEXOS\\4. Percentual de Infrações_complemento.xlsx')

corredores = ['27'] # ['01', '02',...]
subtrechos = ['a', 'b', 'c'] # None

# 5. Heatmap infrações por horário
for corredor in corredores:
    for sentido in [1,2]:
        try:
            if corredor!='27':
                infra_hora = pd.read_csv('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' + corredor +
                                            '\\resumo_analise_hora_dia_trecho_CORRED_' + corredor + '.csv', index_col=[0])
                infra_hora['corredor'] = corredor
                c = infra_hora.at[0, 'corredor']
                infra_hora = infra_hora[infra_hora['sentido'] == sentido]
                infra_hora = infra_hora.sort_values(by='trecho')
                infra_hora = infra_hora.groupby(['hora_dia', 'trecho'])[
                    ['tot_reg_hora', 'infracao_velocidade_media']].sum().reset_index()
                infra_hora['perc_infracao_vel_media'] = round(
                    (infra_hora['infracao_velocidade_media'] / infra_hora['tot_reg_hora']) * 100, 1)
                tot_infra = infra_hora.pivot_table(index='hora_dia', columns='trecho',
                                                   values='infracao_velocidade_media').reset_index()
                registros = infra_hora.pivot_table(index='hora_dia', columns='trecho',
                                                   values='tot_reg_hora').reset_index()
                perc_infra = infra_hora.pivot_table(index='hora_dia', columns='trecho',
                                                    values='perc_infracao_vel_media').reset_index()

                tot_infra[' '] = np.nan
                tot_infra = tot_infra.merge(registros, on='hora_dia', how='left', suffixes=[None, '_registros'])
                tot_infra['   '] = np.nan
                tot_infra = tot_infra.merge(perc_infra, on='hora_dia', how='left',
                                            suffixes=['_infracoes', '_perc_infracoes'])

                print('Salvando Corredor ', corredor, ' sentido ', sentido)
                tot_infra.to_excel('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\ANEXOS\\5.Heatmaps\\' +
                                   'heat_map_infracoes_faixa_horaria_CORRED_' + c + '_sentido_' + str(
                    sentido) + '.xlsx')


            else:
                for subtrecho in subtrechos:
                    infra_hora = pd.read_csv('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\CORRED_' + corredor +
                                                '\\resumo_analise_hora_dia_trecho_CORRED_' + corredor + subtrecho + '.csv', index_col=[0])

                    infra_hora['corredor'] = corredor + subtrecho
                    c = infra_hora.at[0, 'corredor']
                    infra_hora = infra_hora[infra_hora['sentido'] == sentido]
                    infra_hora = infra_hora.sort_values(by='trecho')
                    infra_hora = infra_hora.groupby(['hora_dia', 'trecho'])[
                        ['tot_reg_hora', 'infracao_velocidade_media']].sum().reset_index()
                    infra_hora['perc_infracao_vel_media'] = round(
                        (infra_hora['infracao_velocidade_media'] / infra_hora['tot_reg_hora']) * 100, 1)
                    tot_infra = infra_hora.pivot_table(index='hora_dia', columns='trecho',
                                                       values='infracao_velocidade_media').reset_index()
                    registros = infra_hora.pivot_table(index='hora_dia', columns='trecho',
                                                       values='tot_reg_hora').reset_index()
                    perc_infra = infra_hora.pivot_table(index='hora_dia', columns='trecho',
                                                        values='perc_infracao_vel_media').reset_index()

                    tot_infra[' '] = np.nan
                    tot_infra = tot_infra.merge(registros, on='hora_dia', how='left', suffixes=[None, '_registros'])
                    tot_infra['   '] = np.nan
                    tot_infra = tot_infra.merge(perc_infra, on='hora_dia', how='left',
                                                suffixes=['_infracoes', '_perc_infracoes'])

                    print('Salvando Corredor ', corredor, ' sentido ', sentido)
                    tot_infra.to_excel('D:\\Ciclocidade\\Outputs\\Corredores_Processados\\ANEXOS\\5.Heatmaps\\' +
                                       'heat_map_infracoes_faixa_horaria_CORRED_' + c + subtrecho + '_sentido_' + str(
                        sentido) + '.xlsx')
        except:
            print('Erro no corredor ', corredor)
            continue












